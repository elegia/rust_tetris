// Tetris game logic
// Maarten Lauwers

use opengl_graphics::{ GlGraphics };
use graphics::*;

pub struct Tetris {
    pub lines_completed: u32,
    pub score: u32,
    pub board: Board
}

pub struct Board {
    num_rows: u8,
    num_columns: u8,
    rows: Vec<Vec<Option<Block>>> // Bottom to top (index 0 => 19)
}

pub struct Block {
    size: f64,
    coord: Coord,
    color: [f32; 4]
}

pub struct Shape {
    shapeType: EnumShape,
    blocks: Vec<Block>
}

pub struct Coord {
    row: u8,
    column: u8
}

pub enum EnumShape {
    I,
    J,
    L,
    O,
    S,
    T,
    Z
}

impl Tetris {
    pub fn init() -> Tetris {
        let mut tetris = Tetris{lines_completed: 0, score: 0, board: Board::init()};
        tetris.add_block();
        return tetris;
    }

    pub fn add_block(&mut self) {
        self.board.add_block();
    }
}

impl Board {

    fn init() -> Board {
        let mut board = Board{num_rows: 20, num_columns: 10, rows: Vec::new()};
        let mut rows = Vec::new();
        for _ in 0..board.num_rows {
            let mut columns = Vec::new();
            for _ in 0..board.num_columns {
                columns.push(None);
            }
            rows.push(columns);
        }
        board.rows = rows;
        return board
    }

    pub fn draw(&self, c: Context, gl: &mut GlGraphics) {
        for row in &self.rows {
            for block in row {
                match block {
                    &None => (),
                    &Some(ref b) => b.draw(c, gl),
                }
            }
        }
    }
    
    pub fn add_block(&mut self) {
        let block = Block {size: 32.0, coord: Coord {row: 0, column: 0}, color: [1.0, 0.0, 0.0, 1.0]};
        self.rows[0][0] = Some(block);
    }
}

impl Block {
    // Colors from https://tetris.com/article/88/5-tetris-games-you-should-check-out
    pub fn init(shapeType: &EnumShape, coord: Coord) -> Block {
        let size = 32.0
        match shapeType {
            &EnumShape::I => Block {size: size, coord: coord, color: [0.0, 0.682, 0.878, 1.0]},
            &EnumShape::J => Block {size: size, coord: coord, color: [0.0, 0.365, 0.608, 1.0]},
            &EnumShape::L => Block {size: size, coord: coord, color: [0.988, 0.580, 0.192, 1.0]},
            &EnumShape::O => Block {size: size, coord: coord, color: [1.0, 0.871, 0.184, 1.0]},
            &EnumShape::S => Block {size: size, coord: coord, color: [0.298, 0.706, 0.306, 1.0]},
            &EnumShape::T => Block {size: size, coord: coord, color: [0.576, 0.196, 0.541, 1.0]},
            &EnumShape::Z => Block {size: size, coord: coord, color: [0.953, 0.157, 0.216, 1.0]}
        }
    }

    pub fn draw(&self, c: Context, gl: &mut GlGraphics) {
        use graphics::*;

        let transform = c.transform.trans(0.0, 0.0);
        rectangle(self.color, rectangle::square(0.0, 0.0, self.size), transform, gl);
    }
}

impl Shape {

    pub fn init(shapeType: EnumShape) -> Shape {
        let mut shape = Shape { shapeType: shapeType, blocks: Vec::new() };
        match shape.shapeType {
            EnumShape::I => {
                shape.blocks = vec![
                    Block::init(&shape.shapeType, Coord {row: 0, column: 0}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 1}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 2}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 3})
                ];
            },
            EnumShape::J => {
                shape.blocks = vec![
                    Block::init(&shape.shapeType, Coord {row: 0, column: 0}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 1}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 2}),
                    Block::init(&shape.shapeType, Coord {row: 1, column: 2})
                ];
            },
            EnumShape::L => {
                shape.blocks = vec![
                    Block::init(&shape.shapeType, Coord {row: 0, column: 0}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 1}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 2}),
                    Block::init(&shape.shapeType, Coord {row: 1, column: 0})
                ];
            },
            EnumShape::O => {
                shape.blocks = vec![
                    Block::init(&shape.shapeType, Coord {row: 0, column: 0}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 1}),
                    Block::init(&shape.shapeType, Coord {row: 1, column: 0}),
                    Block::init(&shape.shapeType, Coord {row: 1, column: 1})
                ];
            },
            EnumShape::S => {
                shape.blocks = vec![
                    Block::init(&shape.shapeType, Coord {row: 0, column: 1}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 2}),
                    Block::init(&shape.shapeType, Coord {row: 1, column: 0}),
                    Block::init(&shape.shapeType, Coord {row: 1, column: 1})
                ];
            },
            EnumShape::T => {
                shape.blocks = vec![
                    Block::init(&shape.shapeType, Coord {row: 0, column: 0}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 1}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 2}),
                    Block::init(&shape.shapeType, Coord {row: 1, column: 1})
                ];
            },
            EnumShape::Z => {
                shape.blocks = vec![
                    Block::init(&shape.shapeType, Coord {row: 0, column: 0}),
                    Block::init(&shape.shapeType, Coord {row: 0, column: 1}),
                    Block::init(&shape.shapeType, Coord {row: 1, column: 1}),
                    Block::init(&shape.shapeType, Coord {row: 1, column: 2})
                ];
            }
        }
        return shape;
    }
}
